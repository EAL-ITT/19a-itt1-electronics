![Build Status](https://gitlab.com/EAL-ITT/19a-itt1-electronics/badges/master/pipeline.svg)


# 19A-ITT1-electronics

weekly plans, resources and other relevant stuff for the electronics 1. semester autumn.

public website for students:

*  [gitlab pages](https://eal-itt.gitlab.io/19a-itt1-electronics/)
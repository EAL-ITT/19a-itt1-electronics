---
title: '19A ITT1 electronics'
subtitle: 'Lecture plan'
authors: ['Ilias Esmati \<iles@ucl.dk\>']
main_author: 'Ilias Esmati'
date: \today
email: 'iles@ucl.dk'
left-header: \today
right-header: Lecture plan
skip-toc: false
---


# Lecture plan

The lecture plan consists of the following parts: a week/lecture plan, an overview of the distribution of study activities and general information about the course, module or project.

* Study program, class and semester: IT technology electronics, oeait19, 19A
* Name of lecturer and date of filling in form: ILES 2019-06-01
* Title of the course, module or project and ECTS: Electronics, 5 ECTS
* Required readings, literature or technical instructions and other background material: None

See weekly plan for details like detailed daily plan, links, references, exercises and so on.


-------- ------ ---------------------------------------------
 INIT     Week  Content
-------- ------ ---------------------------------------------

---------------------------------------------------------


# General info about the course, module or project

## The student’s learning outcome


## Content


## Method


## Equipment

None specified at this time as it is dependent on the topic chosen.

## Projects with external collaborators  (proportion of students who participated)

None at this time.

## Test form/assessment
The course includes x compulsory elements.

See exam catalogue for details on compulsory elements.

## Other general information
None at this time.

---
title: 'electroincs'
subtitle: 'exercises'
authors: ['Ilias Esmati \<iles@ucl.dk\>']
main_author: 'Ilias Esmati'
date: \today
email: 'iles@ucl.dk'
left-header: \today
right-header: 'electronics, exercises'
---


Introduction
====================

This document is a collection of exercises. They are associated with the weekly plans. 

References

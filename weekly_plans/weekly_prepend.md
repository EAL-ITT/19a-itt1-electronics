---
title: 'electronics'
subtitle: 'weekly plans'
authors: ['Ilias Esmati \<iles@ucl.dk\>']
main_author: 'Ilias Esmati'
date: \today
email: 'iles@ucl.dk'
left-header: \today
right-header: 'electronics, weekly plans'
---


Introduction
====================

This document is a collection of weekly plans. It is based on the wekly plans in the administrative repository, and is updated automatically on change.

The sections describe the goals and programm for each week of the electronics course.